CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

The Email 2 Auth module provides email two-factor authentication to a Drupal
website.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

The module can be configured at /admin/config/system/email-2-auth. You have the
following settings:
- Roles that will require two-factor authentication
- Expire times
- Email notifications subject and body

MAINTAINERS
-----------

Current maintainers:
 * Albert Jankowski (albertski) - https://www.drupal.org/u/albertski
