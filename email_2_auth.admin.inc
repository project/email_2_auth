<?php

/**
 * @file
 * Email 2 Autth admin settings.
 */

/**
 * Email 2 Auth Admin settings.
 *
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form array.
 */
function email_2_auth_admin(array $form, array $form_state) {
  $user_roles = user_roles();

  $form['email_2_auth_roles'] = [
    '#default_value' => variable_get('email_2_auth_roles', EMAIL_2_AUTH_DEFAULT_ROLES),
    '#options' => $user_roles,
    '#type' => 'checkboxes',
    '#title' => t('Roles'),
    '#description' => t('The roles that should use the email two-factor authentication.'),
  ];

  $form['email_2_auth_expire'] = [
    '#default_value' => variable_get('email_2_auth_expire', EMAIL_2_AUTH_EXPIRE),
    '#type' => 'textfield',
    '#attributes' => [
      ' type' => 'number',
    ],
    '#title' => t('Access Code Expire Time In Minutes'),
    '#description' => t('How many minutes do you want the access token to expire in.'),
  ];

  $form['email_2_auth_ip_expire'] = [
    '#default_value' => variable_get('email_2_auth_ip_expire', EMAIL_2_AUTH_IP_EXPIRE),
    '#type' => 'textfield',
    '#attributes' => [
      ' type' => 'number',
    ],
    '#title' => t('Access Code Expire Time In Seconds'),
    '#description' => t('If a user authenticates and has the same IP, they will not need to enter the access code unless the entered amount of seconds has passed.'),
  ];

  $form['email_2_auth_email_notification_subject'] = [
    '#default_value' => variable_get('email_2_auth_email_notification_subject', EMAIL_2_AUTH_SUBJECT),
    '#type' => 'textfield',
    '#title' => t('Email Notification Subject'),
  ];

  $form['email_2_auth_email_notification_body'] = [
    '#default_value' => variable_get('email_2_auth_email_notification_body', EMAIL_2_AUTH_BODY),
    '#type' => 'textarea',
    '#title' => t('Email Notification Body'),
    '#description' => t('The following tokens are available: [username] and [access_code]'),
  ];

  $form['email_2_auth_bypassed'] = [
    '#default_value' => variable_get('email_2_auth_bypassed'),
    '#type' => 'checkbox',
    '#title' => t('Bypass Email 2 Factor Authentication'),
    '#description' => t('When checked, no users will be required to enter access code.'),
  ];

  $form = system_settings_form($form);

  return $form;
}
