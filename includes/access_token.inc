<?php

/**
 * @file
 * Email 2 Auth User class.
 */

/**
 * Access token functionality.
 */
class Email2AuthAccessToken {

  /**
   * Generate access token.
   *
   * @param object $user
   *   The User object.
   *
   * @return string
   *   Token string.
   */
  public function generate($user) {
    $token = $this->generateToken();
    db_insert('email_2_auth_access')
      ->fields([
        'uid' => $user->uid,
        'code' => $token,
        'expire_date' => date('Y-m-d H:i:s', time() + variable_get('email_2_auth_expire', EMAIL_2_AUTH_EXPIRE) * 60),
      ])
      ->execute();
    return $token;
  }

  /**
   * Validate the code for the user.
   *
   * @param object $user
   *   The User object.
   * @param string $code
   *   Token string.
   *
   * @return mixed
   *   If not valid it will delete empty.
   */
  public function validate($user, $code) {
    return db_query("
      SELECT id
      FROM {email_2_auth_access}
      WHERE uid = :uid
      AND code = :code
      AND expire_date > :current_time
      LIMIT 1
    ", [
      ':uid' => $user->uid,
      ':code' => $code,
      ':current_time' => date('Y-m-d H:i:s'),
    ])->fetchField();
  }

  /**
   * Delete expired access tokens.
   */
  public function deleteExpired() {
    db_delete('email_2_auth_access')
      ->where('expire_date < NOW()')
      ->execute();
  }

  /**
   * Generate token.
   */
  protected function generateToken() {
    return rand(100000, 999999);
  }

}
