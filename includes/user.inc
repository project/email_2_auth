<?php

/**
 * @file
 * Email 2 Auth User class.
 */

/**
 * Email 2 Auth User functionality.
 */
class Email2AuthUser {

  /**
   * User object.
   *
   * @var object
   */
  public $user;

  /**
   * Default constructor.
   *
   * @param object $user
   *   User object.
   */
  public function __construct($user) {
    $this->user = $user;
  }

  /**
   * Check if user has roles.
   *
   * @param array $roles
   *   Roles.
   *
   * @return bool
   *   True if user has role.
   */
  public function userHasRoles(array $roles) {
    if (!$roles) {
      return FALSE;
    }
    else {
      return !!count(array_intersect(is_array($roles) ? $roles : array($roles), array_keys($this->user->roles)));
    }
  }

  /**
   * Check if user has the same IP.
   *
   * @return mixed
   *   Query result.
   */
  public function userHasSameIp() {
    return db_query("
      SELECT id
      FROM {email_2_auth_user}
      WHERE uid = :uid
      AND ip = :ip
      AND last_auth_date > DATE_SUB(NOW(), INTERVAL :seconds SECOND)
      LIMIT 1
    ", [
      ':uid' => $this->user->uid,
      ':ip' => ip_address(),
      ':seconds' => variable_get('email_2_auth_ip_expire', EMAIL_2_AUTH_IP_EXPIRE),
    ])->fetchField();
  }

  /**
   * Save new record.
   */
  public function save() {
    db_merge('email_2_auth_user')
      ->key(['uid' => $this->user->uid])
      ->fields([
        'uid' => $this->user->uid,
        'ip' => ip_address(),
        'last_auth_date' => date('Y-m-d H:i:s'),
      ])
      ->execute();
  }

}
