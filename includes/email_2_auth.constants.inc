<?php

/**
 * @file
 * This file stores any necessary constants for use throughout the module.
 */

define('EMAIL_2_AUTH_DEFAULT_ROLES', []);
define('EMAIL_2_AUTH_EXPIRE', 30);
define('EMAIL_2_AUTH_IP_EXPIRE', 604800);
define('EMAIL_2_AUTH_SUBJECT', "Login Access Code");
define('EMAIL_2_AUTH_BODY', "Hi [username],\n\nYour access code is [access_code].\n\nEnter the code to access your account.\n\nThank you");
