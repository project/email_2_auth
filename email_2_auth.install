<?php

/**
 * @file
 * Email 2 Auth schema functions.
 */

/**
 * Implements hook_schema().
 */
function email_2_auth_schema() {
  $schema['email_2_auth_access'] = [
    'description' => 'Email 2 auth access table.',
    'fields' => [
      'id' => [
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'uid' => [
        'description' => 'The {users}.uid.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'code' => [
        'description' => 'Code',
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => '6',
      ],
      'expire_date' => [
        'description' => 'Expiration date',
        'type' => 'datetime',
        'mysql_type' => 'datetime',
      ],
    ],
    'primary key' => ['id'],
    'indexes' => [
      'uid' => ['uid'],
      'uid_expiration_date' => ['uid', 'expire_date'],
    ],
  ];

  $schema['email_2_auth_user'] = [
    'description' => 'Email 2 auth user table.',
    'fields' => [
      'id' => [
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'uid' => [
        'description' => 'The {users}.uid.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'ip' => [
        'description' => 'Code',
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => '20',
      ],
      'last_auth_date' => [
        'description' => 'Last Authentication date',
        'type' => 'datetime',
        'mysql_type' => 'datetime',
      ],
    ],
    'primary key' => ['id'],
    'indexes' => [
      'uid' => ['uid'],
      'uid_ip' => ['uid', 'ip'],
      'uid_ip_last_login_date' => ['uid', 'ip', 'last_auth_date'],
    ],
  ];

  return $schema;
}

/**
 * Implements hook_uninstall().
 */
function email_2_auth_uninstall() {
  variable_del('email_2_auth_roles');
  variable_del('email_2_auth_expire');
  variable_del('email_2_auth_ip_expire');
  variable_del('email_2_auth_email_notification_subject');
  variable_del('email_2_auth_email_notification_body');
  variable_del('email_2_auth_bypassed');
}
